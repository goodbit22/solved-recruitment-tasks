#!/usr/bin/env python3
"""
Jakie sa wyniki poszczegolnych wywolan funkcji
"""


def f(i, l=[]):
    for i in range(i):
        l.append(i)
    return l  


print(f(2))
# [0,1] 
print(f(3, [1, 2, 3]))
# [1,2,3,0,1,2]


print(f(5))
# [0,1,2,3,4] -> to jest zle 
# [0,1,0,1,2,3,4] 
# -> to prawidlowy wynik jak naprawic to aby  wynik zly  byl zwracany

#problem jest zwiazany ze python przypisuje ta sama tablice
print("rozwiazanie problemu")
def w(i, r = []):
    if r:
        for i in range(i):
            r.append(i)
        return r   
    r = []
    for i in range(i):
        r.append(i)
    return r   

print(w(2))
print(w(3, [1, 2, 3]))
print(w(5))
