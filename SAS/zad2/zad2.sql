#utworzylem sobie przykladowa tabele agd

#zad1
select rok, wojewodztwo, kategoria,  Min(wartosc) min , Max(wartosc)  maks, Sum(wartosc) suma    From agd Group by Rok,Wojewodztwo, Kategoria;
#liczba zwroconych wierszy 96

#zad2
select wojewodztwo, AVG(liczba_sztuk) srednia from agd group by wojewodztwo  Having srednia > (select Avg(liczba_sztuk) from agd); 
#liczba zwroconych wierszy 5  

#zad3
select wojewodztwo , sum(wartosc)  from agd group by wojewodztwo having wojewodztwo IN (select wojewodztwo from (select wojewodztwo , kod from agd group by wojewodztwo  , kod) group by wojewodztwo having count(wojewodztwo) == (select count(*) from (select kod  from agd group by kod)));
#liczba zwroconych wierszy 14
