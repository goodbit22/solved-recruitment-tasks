#zad1.1
find  /var/log/sas  -user sas  ! -perm /g=w -print

#zad1.2
find  /var/log/sas  -user sas  ! -perm /g=w -exec chmod g+w {} \;

#zad1.3
find  /var/log/sas  ! -user sas  -name '*.log'

#zad1.4
find . -type f -exec grep -l 'consul' {} \;

#zad1.5
ls -lAc $(grep -l "ERROR" *) | head -n 5

#zad1.6
sed  's/,/ /g' test.txt

#zad1.7
sed '2 s/serverd.sas.com:7980/serverb.sas.com:8343/' environments.txt

#zad1.8
sort teksty.txt | uniq -c

